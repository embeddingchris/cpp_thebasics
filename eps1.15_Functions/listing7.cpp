/*!
 * @file      listing7.cpp
 * @brief     Surface of a cylinder
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

const double Pi = 3.14159265;

// prototypes: function declarations
double area(double radius);
double circumference(double radius);
double cylinderSurface(double radius, double height);

int main()
{
    double radius = 0.0;
    double height = 0.0;
    
    std::cout << "Enter radius: ";
    std::cin >> radius;
    std::cout << "Enter height: ";
    std::cin >> height;

    // call function "cylinderSurface"
    std::cout << "The surface of the cylinder is: " << cylinderSurface(radius, height) << std::endl;

    return 0;
}

// implementation: function definition
double area(double radius)
{
    return Pi * radius * radius;
}

double circumference(double radius)
{
    return 2 * Pi * radius;
}

double cylinderSurface(double radius, double height)
{
    // call functions "area" and "circumference"
    return 2 * area(radius) + circumference(radius) * height;
}