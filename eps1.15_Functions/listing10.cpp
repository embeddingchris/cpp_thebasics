/*!
 * @file      listing10.cpp
 * @brief     Function parameter with default value
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

double area(double radius, double Pi = 3.14159265);