/*!
 * @file      listing11.cpp
 * @brief     Function parameter with default value
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

// prototype: function declarations
double area(double radius, double Pi = 3.14159265);

int main()
{
    double radius = 0.0;

    std::cout << "Enter radius: ";
    std::cin >> radius;

    std::cout << "Do you want to have a different value of pi? (y/n) ";
    char charPi;
    std::cin >> charPi;

    if( charPi == 'y')
    {
        std::cout << "Enter new Pi value: ";
        double newPi;
        std::cin >> newPi;

        // call function "area with second argument"
        std::cout << "The area is: " << area(radius, newPi) << std::endl;
    }
    else
    {
        // call function "area without second argument"
        std::cout << "The area is: " << area(radius) << std::endl;
    }
    
    return 0;
}

// implementation: function definition
double area(double radius, double Pi)
{
    return Pi * radius * radius;
}