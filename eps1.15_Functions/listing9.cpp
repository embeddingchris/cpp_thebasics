/*!
 * @file      listing9.cpp
 * @brief     Empty return statement
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

void welcome()
{
    std::cout << "Welcome!" << std::endl;

    return; // empty return statement
}