/*!
 * @file      listing2.cpp
 * @brief     Function prototype
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

// |Return value    |Function parameters
    double diameter (double radius);
//         |Function name