/*!
 * @file      listing3.cpp
 * @brief     Function definition
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

double diameter (double radius)
{
    return 2 * radius;
}