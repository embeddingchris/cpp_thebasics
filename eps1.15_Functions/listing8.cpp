/*!
 * @file      listing8.cpp
 * @brief     Function with no parameters and no retrun values
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

// prototype: function declaration
void welcome();

int main()
{
    welcome();

    return 0;
}

// implementation: function definition
void welcome()
{
    std::cout << "Welcome!" << std::endl;
}