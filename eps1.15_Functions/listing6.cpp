/*!
 * @file      listing6.cpp
 * @brief     Function with multiple parameters
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

// prototype: function declaration
double cylinderSurface(double radius, double height);

// implementation: function definition
double cylinderSurface(double radius, double height)
{
    return 2 * radius * radius + 2 * radius * radius + 2 * radius * height;
}