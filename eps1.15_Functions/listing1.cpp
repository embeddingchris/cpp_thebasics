/*!
 * @file      listing1.cpp
 * @brief     Computing the area and circumference of a circle with given radius
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-27
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

const double Pi = 3.14159265;

// functions
double diameter(double radius)
{
    return 2 * radius;
}

double area(double radius)
{
    return Pi * radius * radius;
}

double circumference(double radius)
{
    return 2 * Pi * radius;
};

// main function
int main()
{
    double radius = 0.0;
    
    std::cout << "Enter radius: ";
    std::cin >> radius;
    
    // call function "diameter"
    std::cout << "diameter is: " << diameter(radius) << std::endl;
    
    // call function "area"
    std::cout << "area is: " << area(radius) << std::endl;
    
    // call function "circumference"
    std::cout << "circumference is: " << circumference(radius) << std::endl;
    
    return 0;
}