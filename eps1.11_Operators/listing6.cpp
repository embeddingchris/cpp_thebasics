/*!
 * @file      listing6.cpp
 * @brief     Bitshift
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-01
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <bitset>

int main() {
    short operand1 = 7;
    short operand2 = 4;

    std::cout << "a decimal: " << operand1 << " binary: " << std::bitset<8>(operand1) << std::endl;
    // operand1<<2
    std::cout << "a<<2 decimal: "<< (operand1<<2) << " binary: " << std::bitset<8>(operand1<<2) << std::endl;
    // operand1>>1
    std::cout << "a>>1 decimal: "<< (operand1>>1) << " binary: " << std::bitset<8>(operand1>>1) << std::endl << std::endl;

    std::cout << "b decimal: " << operand2 << " binary: " << std::bitset<8>(operand2) << std::endl;
    // operand2<<2
    std::cout << "b<<2 decimal: "<< (operand2<<1) << " binary: " << std::bitset<8>(operand2<<1) << std::endl;
    // operand2>>1
    std::cout << "b>>1 decimal: "<< (operand2>>1) << " binary: " << std::bitset<8>(operand2>>1) << std::endl;

    return 0;
}