/*!
 * @file      listing1.cpp
 * @brief     Arithmetic operators
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-01
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main() {
    int operand1 = 5;
    int operand2 = 2;

    std::cout << operand1 << " + " << operand2 << " = " << operand1 + operand2 << std::endl; // add
    std::cout << operand1 << " - " << operand2 << " = " << operand1 - operand2 << std::endl; // subtract
    std::cout << operand1 << " * " << operand2 << " = " << operand1 * operand2 << std::endl; // multiply
    std::cout << operand1 << " / " << operand2 << " = " << operand1 / operand2 << std::endl; // divide, division of integer causes lost of remainder
    std::cout << operand1 << " % " << operand2 << " = " << operand1 % operand2 << std::endl; // modulo

    return 0;
}