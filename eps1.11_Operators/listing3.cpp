/*!
 * @file      listing3.cpp
 * @brief     Comparison and relational operators
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-01
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main() {
    int operand1 = 5;
    int operand2 = 2;

    std::cout << operand1 << " is equal to " << operand2 << " : " << (operand1 == operand2) << std::endl;
    std::cout << operand1 << " is not equal to " << operand2 << " : " << (operand1 != operand2) << std::endl;
    std::cout << operand1 << " is greater than " << operand2 << " : " << (operand1 > operand2) << std::endl;
    std::cout << operand1 << " is less than " << operand2 << " : " << (operand1 < operand2) << std::endl;
    std::cout << operand1 << " is greater than or equal to " << operand2 << " : " << (operand1 >= operand2) << std::endl;
    std::cout << operand1 << " is less than or equal to " << operand2 << " : " << (operand1 <= operand2) << std::endl << std::endl;

    std::cout << " true == 1 -> " << (true == 1) << std::endl;
    std::cout << " false == 0 -> " << (false == 0) << std::endl;

    return 0;
}