/*!
 * @file      listing4.cpp
 * @brief     Logical operators
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-01
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main() {
    bool operand1 = true;
    bool operand2 = false;

    std::cout << "|  Table of truth  |" << std::endl;
    std::cout << "| ---------------- |" << std::endl;
    std::cout << "| a | b | and | or |" << std::endl;
    std::cout << "| ---------------- |" << std::endl;
    std::cout << "| " << operand1 << " | "<< operand1 << " |  "<< (operand1 && operand1) << "  |  "<< (operand1 || operand1) << " |"<< std::endl;
    std::cout << "| " << operand1 << " | "<< operand2 << " |  "<< (operand1 && operand2) << "  |  "<< (operand1 || operand2) << " |"<< std::endl;
    std::cout << "| " << operand2 << " | "<< operand1 << " |  "<< (operand2 && operand1) << "  |  "<< (operand2 || operand1) << " |"<< std::endl;
    std::cout << "| " << operand2 << " | "<< operand2 << " |  "<< (operand2 && operand2) << "  |  "<< (operand2 || operand2) << " |"<< std::endl;
    std::cout << std::endl;
    std::cout << "| a = " << operand1 << " | not a = " << !operand1 << " |"<< std::endl;
    std::cout << "| b = " << operand2 << " | not b = " << !operand2 << " |"<< std::endl;

    return 0;
}