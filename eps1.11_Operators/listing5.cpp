/*!
 * @file      listing5.cpp
 * @brief     Bitwise operators
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-01
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <bitset>

int main() {
    short operand1 = 7;
    short operand2 = 4;

    std::cout << "a decimal: "<< operand1 << " binary: " << std::bitset<8>(operand1) << std::endl;
    std::cout << "b decimal: "<< operand2 << " binary: " << std::bitset<8>(operand2) << std::endl << std::endl;

    unsigned short operand3 = ~operand1;
    short operand4 = ~operand2;

    std::cout << "~a decimal: "<< (operand3) << " binary: " << std::bitset<8>(operand3) << std::endl;
    std::cout << "~b decimal: "<< (operand4) << " binary: " << std::bitset<8>(operand4) << std::endl << std::endl;

    std::cout << "a & b decimal: "<< (operand1 & operand2) << " binary: " << std::bitset<8>(operand1 & operand2) << std::endl;
    std::cout << "a | b decimal: "<< (operand1 | operand2) << " binary: " << std::bitset<8>(operand1 | operand2) << std::endl;
    std::cout << "a ^ b decimal: "<< (operand1 ^ operand2) << " binary: " << std::bitset<8>(operand1 ^ operand2) << std::endl << std::endl;

    return 0;
}