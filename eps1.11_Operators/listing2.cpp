/*!
 * @file      listing2.cpp
 * @brief     Increment and decrement
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-01
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main() {
    int operand1 = 5;
    int operand2 = 2;

    std::cout << "x = " << operand1 << " y = " << operand2 << std::endl;
    std::cout << "++x = " << ++operand1 << " --y = " << --operand2 << std::endl;

    return 0;
}