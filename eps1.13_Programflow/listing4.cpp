/*!
 * @file      listing4.cpp
 * @brief     Switch case
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-29
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <stdlib.h>

int main()
{
    enum numbers                // enumeration for naming the switch cases
    {
        one,
        two,
        three,
        four,
        five,
        six
    };

    int dice = rand() % 6 + 1;  // dice throw, random number

    switch(dice)
    {
        case one:
            std::cout << "the dice counts one" << std::endl;
            break;
        case two:
            std::cout << "the dice counts two" << std::endl;
            break;
        case three:
            std::cout << "the dice counts three" << std::endl;
            break;
        case four:
            std::cout << "the dice counts four" << std::endl;
            break;
        case five:
            std::cout << "the dice counts five" << std::endl;
            break;
        case six:
            std::cout << "the dice counts six" << std::endl;
            break;
        default:
            std::cout << "Tell me the default state of a dice" << std::endl;
    }

    return 0;
}