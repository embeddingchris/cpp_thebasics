/*!
 * @file      listing5.cpp
 * @brief     Conditional operator
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-29
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int val1 {0};
    int val2 {0};

    std::cout << "Please, enter a number: " << std::endl;
    std::cin >> val1;

    std::cout << "Please, enter a second number: " << std::endl;
    std::cin >> val2;

    int greatVal = (val1 > val2)? val1 : val2;      // conditional operator

    std::cout << greatVal << " is the greater of both numbers" << std::endl;

    return 0;
}