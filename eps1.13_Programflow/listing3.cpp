/*!
 * @file      listing3.cpp
 * @brief     Grouped if else
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-29
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <stdlib.h>

int main()
{
    int dice = rand() % 6 + 1;

    if(dice == 1)
    {
        std::cout << "the dice counts one" << std::endl;
    }
    else if(dice == 2)
    {
        std::cout << "the dice counts two" << std::endl;
    }
    else if(dice == 3)
    {
        std::cout << "the dice counts three" << std::endl;
    }
    else if(dice == 4)
    {
        std::cout << "the dice counts four" << std::endl;
    }
    else if(dice == 5)
    {
        std::cout << "the dice counts five" << std::endl;
    }
    else
    {
        std::cout << "the dice counts six" << std::endl;
    }
    
    return 0;
}