/*!
 * @file      listing2.cpp
 * @brief     Nested if
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-29
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <stdlib.h>

int main()
{
    bool condition1 = rand() % 2;
    std::cout << "condition is " << condition1 << std::endl;
    
    bool condition2 = rand() % 2;
    std::cout << "condition is " << condition2 << std::endl;
    
    if(condition1)
    {
        if(condition2)
        {
            std::cout << "condition1 and condition2 is true" << std::endl;
        }
        else
        {
            std::cout << "only condition1 is true" << std::endl;
        }
    }
    else
    {
        if(condition2)
        {
            std::cout << "only condition2 is true" << std::endl;
        }
        else
        {
            std::cout << "no condition is true" << std::endl;
        }
    }
    
    return 0;
}