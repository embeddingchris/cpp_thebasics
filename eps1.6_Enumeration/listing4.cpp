/*!
 * @file      listing4.cpp
 * @brief     Implicit converting to integer
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

// enum
enum colorRGB { red, green, blue };

int i = red + blue;     // equivalent to 0 + 2

// enum class
enum class colorCMYK {cyan, magenta, yellow, key};

int j = colorCMYK::cyan + colorCMYK::magenta; // error