/*!
 * @file      listing1.cpp
 * @brief     Enums syntax
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

enum class TypName
{
    ListOfValues;
} ListOfVariables;