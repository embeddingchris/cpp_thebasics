/*!
 * @file      listing7.cpp
 * @brief     Explicit casting
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int dividend = 3;
    int divisor = 4;

    float quotient = dividend/divisor;              // result = 0
    std::cout << " Without explicit cast: " << quotient << std::endl;

    float quotientCast = (float)dividend/divisor;   // result 0.75
    std::cout << " With explicit cast: " << quotientCast << std::endl;

    return 0;
}