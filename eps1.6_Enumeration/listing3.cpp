/*!
 * @file      listing3.cpp
 * @brief     Enumeration with custom interger values
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

enum class cardinalDirection
{
    North   =   78,
    East    =    6,
    South   =    0,
    West    =   -1
} myDirection;