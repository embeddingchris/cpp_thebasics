/*!
 * @file      listing2.cpp
 * @brief     CardinalDirection enumeration
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

enum class cardinalDirection
{
    North,  // = 0
    East,   // = 1
    South,  // = 2
    West    // = 3
} myDirection;

int main()
{
    myDirection = cardinalDirection::East;

    return 0;
}