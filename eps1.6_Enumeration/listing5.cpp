/*!
 * @file      listing5.cpp
 * @brief     Comparison enum and enum class
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    // enum
    enum colorRGB
    {
        red,
        green,
        blue
    } colorAdd;

    colorAdd = red; // assign

    int i = colorAdd; // implicit convertion

    colorAdd = (colorRGB)2; // old style casting

    std::cout << "Converted Interger: " << i << " Convereted Enum: " << colorAdd << std::endl; // cout of the enum, impicit convertion

    // enum class
    enum class colorCMYK
    {
        cyan,
        magenta,
        yellow,
        key
    } colorSub;

    colorSub = colorCMYK::key; // assign

    int j = static_cast<int>(colorSub); // casting to int

    colorSub = static_cast<colorCMYK>(1); // casting to enum class

    std::cout << "Converted Interger: " << j << " Convereted Enum: " << static_cast<int>(colorSub) << std::endl; // cout of the enum class

    return 0;
}