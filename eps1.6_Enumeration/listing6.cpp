/*!
 * @file      listing6.cpp
 * @brief     Implicit casting
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    float f = 1.5;
    int i = f;      // implicit cast

    std::cout << "Float " << f << " converts to Int " << i << std::endl;

    return 0;
}