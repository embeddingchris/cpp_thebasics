/*!
 * @file      listing8.cpp
 * @brief     Syntax of the casting operators
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-06-23
 * 
 * @copyright GNU General Public License v3.0 
 */

destination_type result = cast_operator<destination_type> (object_to_cast);