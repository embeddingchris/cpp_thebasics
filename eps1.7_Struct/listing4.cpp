/*!
 * @file      listing4.cpp
 * @brief     Padding of structs
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-07
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

struct structA
{
  short anInt;
  float aFloat;
  char aChar;
};

struct structB
{
  float aFloat;
  char aChar;
  short anInt;
};

struct structC
{
  char aChar;
  short anInt;
  float aFloat;
};

int main()
{  
  std::cout << "Size of short: " << sizeof(short) << " bytes" << std::endl  // output = 2 bytes
  << "Size of char: " << sizeof(char) << " bytes" << std::endl              // output = 1 bytes
  << "Size of float: " << sizeof(float) << " bytes\n" << std::endl;         // output = 4 bytes

  std::cout << "Size of structA: " << sizeof(structA) << " bytes" << std::endl;  // output = 12 bytes
  std::cout << "Size of structB : " << sizeof(structB) << " bytes" << std::endl; // output = 8 bytes
  std::cout << "Size of structC : " << sizeof(structC) << " bytes" << std::endl; // output = 8 bytes

  return 0;
}