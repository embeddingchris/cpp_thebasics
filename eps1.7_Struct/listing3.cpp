/*!
 * @file      listing3.cpp
 * @brief     Size of enum colorRGB and struct subPixel
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-07
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

enum class colorRGB
{
  red,   // equivalent to 0
  green, // equivalent to 1
  blue   // equivalent to 2
} myColor;

struct subPixel
{
  int intensity;
  colorRGB color;
} aSubpixel;

int main()
{
  std::cout << "enum" << std::endl;
  std::cout << "Size of int: " << sizeof(int) << " bytes" << std::endl;       // output = 4 bytes
  std::cout << "Size of enum : " << sizeof(myColor) << " bytes" << std::endl; // output = 4 bytes

  std::cout << std::endl << "struct" << std::endl;
  std::cout << "Size of int element: " << sizeof(aSubpixel.intensity) << " bytes" << std::endl;       // output = 4 bytes
  std::cout << "Size of enum element: " << sizeof(aSubpixel.color) << " bytes" << std::endl; // output = 4 bytes
  std::cout << "Size of struct : " << sizeof(aSubpixel) << " bytes" << std::endl; // output = 8 bytes

  return 0;
}