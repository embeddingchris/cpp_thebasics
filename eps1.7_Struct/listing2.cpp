/*!
 * @file      listing2.cpp
 * @brief     Example structure pixel
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-07
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

enum class colorRGB
{
  red,   // equivalent to 0
  green, // equivalent to 1
  blue   // equivalent to 2
};

struct subPixel
{
  int intensity;
  colorRGB color;
};

struct pixel
{
    int x;
    int y;

    subPixel redSubPixel;
    subPixel greenSubPixel;
    subPixel blueSubPixel;
}aPixel;

int main()
{
  aPixel.x = 24;
  aPixel.y = 201;

  aPixel.redSubPixel.intensity = 136;
  aPixel.redSubPixel.color = colorRGB::red;

  aPixel.greenSubPixel.intensity = 76;
  aPixel.greenSubPixel.color = colorRGB::green;

  aPixel.blueSubPixel.intensity = 198;
  aPixel.blueSubPixel.color = colorRGB::blue;

  std::cout << "Position x: " << aPixel.x << " y: " << aPixel.y << std::endl;
  std::cout << "Red subpixel intesity: " << aPixel.redSubPixel.intensity
            << " color: " << static_cast<int>(aPixel.redSubPixel.color) << std::endl;
  std::cout << "Green subpixel intesity: " << aPixel.greenSubPixel.intensity
            << " color: " << static_cast<int>(aPixel.greenSubPixel.color) << std::endl;
  std::cout << "Blue subpixel intesity: " << aPixel.blueSubPixel.intensity
            << " color: " << static_cast<int>(aPixel.blueSubPixel.color) << std::endl;

  return 0;
}