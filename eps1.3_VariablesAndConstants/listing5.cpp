/*!
 * @file      listing5.cpp
 * @brief     Definition of a constant
 * @details   Show the definition of a constant
 * 
 * @author    embeddingchris
 * @date      2019-05-12
 * 
 * @copyright GNU General Public License v3.0 
 */

// const datatype identifier = value;
const float pi = 3.14159265359; 