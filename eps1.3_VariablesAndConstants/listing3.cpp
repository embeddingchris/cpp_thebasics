/*!
 * @file      listing3.cpp
 * @brief     Definition of a variable
 * @details   Show the definition of a variable
 * 
 * @author    embeddingchris
 * @date      2019-05-12
 * 
 * @copyright GNU General Public License v3.0 
 */

// datatype identifier = value;
float radius = 3.0; 