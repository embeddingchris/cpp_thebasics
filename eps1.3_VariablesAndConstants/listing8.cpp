/*!
 * @file      listing8.cpp
 * @brief     Definitions
 * @details   Show examples of definitions
 * 
 * @author    embeddingchris
 * @date      2019-05-12
 * 
 * @copyright GNU General Public License v3.0 
 */

// initialization
int aNumber {6};    

// definition
double aFunction()  
{
    return -3.5;
};

// definition
double aMethod(int i, double d) 
{
    return i + d;
};