/*!
 * @file      listing6.cpp
 * @brief     Literals
 * @details   Show examples of literals
 * 
 * @author    embeddingchris
 * @date      2019-05-12
 * 
 * @copyright GNU General Public License v3.0 
 */

int aNumber = 2;
int aHexValue = 0x10;
char aCharacter = 'C';