/*!
 * @file      listing7.cpp
 * @brief     Declarations
 * @details   Show examples of declarations
 * 
 * @author    embeddingchris
 * @date      2019-05-12
 * 
 * @copyright GNU General Public License v3.0 
 */

int aNumber;                        // variable with integer datatype
double aFunction();                 // function with double return value
double aMethod(int i, double d);    // function with arguments