/*!
 * @file      listing1.cpp
 * @brief     Compound assignment operators
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-15
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <bitset>

int main() {
    int operand1 = 7;
    int operand2 = 4;

    // +=
    std::cout << "operand1: " << operand1 << " operand2: " << operand2 << std::endl;
    std::cout << "operand1 = operand1 + operand2: " << (operand1 + operand2) << std::endl;

    operand1 += operand2;
    std::cout << " operand1 += operand2: " << operand1 << std::endl << std::endl;

    // -=
    std::cout << "operand1: " << operand1 << " operand2: " << operand2 << std::endl;
    std::cout << "operand1 = operand1 - operand2: " << (operand1 - operand2) << std::endl;

    operand1 -= operand2;
    std::cout << "operand1 -= operand2: " << operand1 << std::endl << std::endl;

    // *=
    std::cout << "operand1: " << operand1 << " operand2: " << operand2 << std::endl;
    std::cout << "operand1 = operand1 * operand2: " << (operand1 * operand2) << std::endl;

    operand1 *= operand2;
    std::cout << "operand1 *= operand2: " << operand1 << std::endl << std::endl;

    // /=
    std::cout << "operand1: " << operand1 << " operand2: " << operand2 << std::endl;
    std::cout << "operand1 = operand1 / operand2: " << (operand1 / operand2) << std::endl;

    operand1 /= operand2;
    std::cout << " operand1 /= operand2: " << operand1  << std::endl << std::endl;

    // %=
    std::cout << "operand1: " << operand1 << " operand2: " << operand2 << std::endl;
    std::cout << "operand1 = operand1 % operand2: " << (operand1 % operand2) << std::endl;

    operand1 %= operand2;
    std::cout << "operand1 %= operand2: " << operand1 << std::endl << std::endl;

    // &=
    std::cout << "operand1: " << std::bitset<8>(operand1) << " operand2: " << std::bitset<8>(operand2) << std::endl;
    std::cout << "operand1 = operand1 & operand2: " << std::bitset<8>(operand1 & operand2) << std::endl;

    operand1 &= operand2;
    std::cout << "operand1 &= operand2: " << std::bitset<8>(operand1) << std::endl << std::endl;

    // |=
    std::cout << "operand1: " << std::bitset<8>(operand1) << " operand2: " << std::bitset<8>(operand2) << std::endl;
    std::cout << "operand1 = operand1 | operand2: " << std::bitset<8>(operand1 | operand2) << std::endl;

    operand1 |= operand2;
    std::cout << "operand1 |= operand2: " << std::bitset<8>(operand1) << std::endl << std::endl;

    operand1 = 5;
    operand2 = 2;

    // ^=
    std::cout << "operand1: " << std::bitset<8>(operand1) << " operand2: " << std::bitset<8>(operand2) << std::endl;
    std::cout << "operand1 = operand1 ^ operand2: " << std::bitset<8>(operand1 ^ operand2) << std::endl;

    operand1 ^= operand2;
    std::cout << "operand1 ^= operand2: " << std::bitset<8>(operand1) << std::endl << std::endl;

    // <<=
    std::cout << "operand1: " << std::bitset<8>(operand1) << " operand2: " << std::bitset<8>(operand2) << std::endl;
    std::cout << "operand1 = operand1 << operand2: " << std::bitset<8>(operand1 << operand2) << std::endl;

    operand1 <<= operand2;
    std::cout << "operand1 <<= operand2: " << std::bitset<8>(operand1) << std::endl << std::endl;

    // >>=
    std::cout << "operand1: " << std::bitset<8>(operand1) << " operand2: " << std::bitset<8>(operand2) << std::endl;
    std::cout << "operand1 = operand1 >> operand2: " << std::bitset<8>(operand1 >> operand2) << std::endl;

    operand1 >>= operand2;
    std::cout << "operand1 >>= operand2: " << std::bitset<8>(operand1) << std::endl << std::endl;

    return 0;
}