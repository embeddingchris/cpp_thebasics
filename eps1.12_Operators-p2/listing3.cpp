/*!
 * @file      listing3.cpp
 * @brief     Operator precedence
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-15
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int result = 11*3+20-5*5 << 1;
    std::cout << "11*3+20-5*5 << 1 =" << result << std::endl;

    result = 33+20-25 << 1;
    std::cout << "33+20-25 << 1 =" << result << std::endl;

    result = 28 << 1;
    std::cout << "28 << 1 =" << result << std::endl;

    result = (11*3)+20-(5*5) << 1;
    std::cout << "(11*3)+20-(5*5) << 1 =" << result << std::endl;

    return 0;
}