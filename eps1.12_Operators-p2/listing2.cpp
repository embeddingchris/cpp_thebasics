/*!
 * @file      listing2.cpp
 * @brief     Prefix and postfix
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-09-15
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int operand1 = 0;
    int operand2 = 4;

    std::cout << "operand1 = " << operand1 << " operand2 = " << operand2 << std::endl;

    // prefix increment
    operand1 = ++operand2;

    std::cout << "after prefix increment" << std::endl;
    std::cout << "operand1 = " << operand1 << " operand2 = " << operand2 << std::endl;

    // prefix decrement
    operand1 = --operand2;

    std::cout << "after prefix decrement" << std::endl;
    std::cout << "operand1 = " << operand1 << " operand2 = " << operand2 << std::endl;

    // postfix increment
    operand1 = operand2++;

    std::cout << "after postfix increment" << std::endl;
    std::cout << "operand1 = " << operand1 << " operand2 = " << operand2 << std::endl;

    // postfix decrement
    operand1 = operand2--;

    std::cout << "after postfix decrement" << std::endl;
    std::cout << "operand1 = " << operand1 << " operand2 = " << operand2 << std::endl;

    return 0;
}