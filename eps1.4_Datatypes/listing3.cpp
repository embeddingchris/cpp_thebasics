/*!
 * @file      listing3.cpp
 * @brief     Static type
 * @details   Show the static type in C++
 * 
 * @author    embeddingchris
 * @date      2019-05-26
 * 
 * @copyright GNU General Public License v3.0 
 */

int year = 2019;                            // int
std::string firstName = "embeddingchris";   // std::string