/*!
 * @file      listing5.cpp
 * @brief     Strongly typed
 * @details   Show strong type safety in C++
 * 
 * @author    embeddingchris
 * @date      2019-05-26
 * 
 * @copyright GNU General Public License v3.0 
 */

int length = 2.54;  // compiling error
size = (int)2.54;   // int size = 2