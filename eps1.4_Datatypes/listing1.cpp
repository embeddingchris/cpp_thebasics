/*!
 * @file      listing1.cpp
 * @brief     Definition of integer and floating point variables
 * @details   Show how to define integer and floating point variables
 * 
 * @author    embeddingchris
 * @date      2019-05-26
 * 
 * @copyright GNU General Public License v3.0 
 */

int integerNumber = 7;
float realNumber = 7.0;