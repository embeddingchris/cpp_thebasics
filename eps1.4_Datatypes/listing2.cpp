/*!
 * @file      listing2.cpp
 * @brief     Size of primitive datatypes
 * @details   Show the size of some basic datatypes
 * 
 * @author    embeddingchris
 * @date      2019-05-26
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    std::cout << "Size of bool = "  << sizeof(bool)     << " Bytes" << std::endl;
    std::cout << "Size of char = "  << sizeof(char)     << " Bytes" << std::endl;
    std::cout << "Size of int = "   << sizeof(int)      << " Bytes" << std::endl;
    std::cout << "Size of float = " << sizeof(float)    << " Bytes" << std::endl;

    return 0;
}