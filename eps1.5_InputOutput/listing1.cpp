/*!
 * @file      listing1.cpp
 * @brief     Hello world
 * @details   Simple hello world program
 * 
 * @author    embeddingchris
 * @date      2019-06-09
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    std::cout << "Hello World!" << std::endl;
    return 0;
}