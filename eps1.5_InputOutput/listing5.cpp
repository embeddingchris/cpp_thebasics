/*!
 * @file      listing5.cpp
 * @brief     endl or \n
 * @details   Linebreak variants
 * 
 * @author    embeddingchris
 * @date      2019-06-09
 * 
 * @copyright GNU General Public License v3.0 
 */

std::cout << std::endl  // inserts a new line and flushes the stream

std::cout << "\n"       // only inserts a new line.