/*!
 * @file      listing3.cpp
 * @brief     Chained statements
 * @details   Chained user input of integer variables
 * 
 * @author    embeddingchris
 * @date      2019-06-09
 * 
 * @copyright GNU General Public License v3.0 
 */

cin >> a >> b;