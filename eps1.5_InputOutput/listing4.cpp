/*!
 * @file      listing4.cpp
 * @brief     Input of an integer value
 * @details   Complete example of input of an integer value
 * 
 * @author    embeddingchris
 * @date      2019-06-09
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    char firstLetter;
    float floatNumber;
    int intNumber;

    std::cout << "Hi, what is the first letter of your name?" << std::endl;
    std::cin >> firstLetter;

    std::cout << "And now, please tell me a number between 2.0 and 3.0" << std::endl;
    std::cin >> floatNumber;

    intNumber = floatNumber;

    std::cout << "Hey " << firstLetter << ", be careful with the data types, otherwise your " << floatNumber << " will quickly turn into a " << intNumber << "!" << std::endl;

    return 0;
}