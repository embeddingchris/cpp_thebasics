/*!
 * @file      listing2.cpp
 * @brief     Input of an integer value
 * @details   Declaration and user input of an integer variable
 * 
 * @author    embeddingchris
 * @date      2019-06-09
 * 
 * @copyright GNU General Public License v3.0 
 */

int value;
cin >> value;