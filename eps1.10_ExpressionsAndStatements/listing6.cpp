/*!
 * @file      listing6.cpp
 * @brief     Expression statement
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-18
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int anInt;                       // declaration statement

    anInt = 3;                       // expression statement

    std::cout << anInt << std::endl; // expression statement

    return 0;                        // expression statement
}