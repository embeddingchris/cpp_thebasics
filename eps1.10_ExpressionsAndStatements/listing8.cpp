/*!
 * @file      listing8.cpp
 * @brief     Initialising and assigning
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-18
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <vector>

int main()
{
    int i1;                             // declaration
    i1 = 7;                             // assigning

    double d1 = 1.5;                    // c-stlye initializing
    double d2 {1.5};                    // initializing with initializer list

    std::vector<int> v1 {1,2,3,4,5,6};  // initializing with initializer list

    int i2 = 7.8;                       // i2 implicitly converts to 7
    int i3 {7.8};                       // error

    return 0;
}