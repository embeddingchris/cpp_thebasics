/*!
 * @file      listing2.cpp
 * @brief     Conditional expression
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-18
 * 
 * @copyright GNU General Public License v3.0 
 */

c = (a > b) ? a : b