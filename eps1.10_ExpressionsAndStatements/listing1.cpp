/*!
 * @file      listing1.cpp
 * @brief     Expressions
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-18
 * 
 * @copyright GNU General Public License v3.0 
 */

a = 7

7 - 4

b = 12 + a