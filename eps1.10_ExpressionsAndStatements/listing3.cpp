/*!
 * @file      listing3.cpp
 * @brief     Statement
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-18
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int a;                          // declaration statement

    a = 13 + 7;                     // expression statement

    std::cout << a << std::endl;    // expression statement

    return 0;
}