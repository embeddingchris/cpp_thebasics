/*!
 * @file      listing4.cpp
 * @brief     Strings over two lines
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-18
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    std::cout << "Hello
    World" << std::endl;     // error

    std::cout << "Hello \
    World" << std::endl;     // valid

    std::cout << "Hello "
    "World" << std::endl;    // valid

    return 0;
}