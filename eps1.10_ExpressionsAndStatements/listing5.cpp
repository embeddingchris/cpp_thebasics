/*!
 * @file      listing5.cpp
 * @brief     Declaration statement
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-18
 * 
 * @copyright GNU General Public License v3.0 
 */

int main()
{
    int anInt; // declaration statement

    return 0;
}