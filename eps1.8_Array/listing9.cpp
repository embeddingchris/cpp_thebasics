/*!
 * @file      listing9.cpp
 * @brief     Escape code "\0" in char arrays
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    char sayHello[] = {'H','e','l','l','o',' ','W','o','r','l','d','\0'};
    std::cout << sayHello << std::endl;
    std::cout << "Size of array: " << sizeof(sayHello) << std::endl;

    std::cout << "Replacing space with null" << std::endl;
    sayHello[5] = '\0';
    std::cout << sayHello << std::endl;
    std::cout << "Size of array: " << sizeof(sayHello) << std::endl;
    return 0;
}