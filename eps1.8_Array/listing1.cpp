/*!
 * @file      listing1.cpp
 * @brief     Syntax of an array
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

datatype arrayName [numberOfElements];