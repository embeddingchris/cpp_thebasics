/*!
 * @file      listing8.cpp
 * @brief     Char array
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    std::cout << "Hello World" << std::endl; // string literal

    char sayHello[] = {'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', '\0'}; // initialize char array
    std::cout << sayHello << std::endl;

    return 0;
}