/*!
 * @file      listing5.cpp
 * @brief     Accessing data of an array
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

struct vinyl
{
    // elements
};

int main()
{
    vinyl lp1;  // declaring variable of type vinyl
    vinyl lp2;
    vinyl lp3;
    vinyl lp4;
    vinyl lp5;

    const int numberOfVinyls {5};
    vinyl lpCollection[numberOfVinyls];

    lpCollection[0] = lp1;  // assigning element at index 0
    lpCollection[1] = lp2;
    lpCollection[2] = lp3;
    lpCollection[3] = lp4;
    lpCollection[4] = lp5;

    vinyl actualElement = lpCollection[3];  // accessing element at index 3

    return 0;
}