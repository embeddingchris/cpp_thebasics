/*!
 * @file      listing3.cpp
 * @brief     Array of some structs
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

struct vinyl
{
    // elements
};

int main()
{
    const int numberOfVinyls {5};       // initializing integer
    vinyl lpCollection[numberOfVinyls]; // declaring array

    return 0;
}