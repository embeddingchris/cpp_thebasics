/*!
 * @file      listing6.cpp
 * @brief     Size of an array
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

int numberOfElements = sizeof(anArray);
int sizeOfArray = sizeof(elementType) * numberOfElements; // in bytes