/*!
 * @file      listing2.cpp
 * @brief     Declaration of some structs
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

struct vinyl
{
    // elements
};

int main()
{
    vinyl lp1;  // declaring variable of type vinyl
    vinyl lp2;
    vinyl lp3;
    vinyl lp4;
    vinyl lp5;

    return 0;
}