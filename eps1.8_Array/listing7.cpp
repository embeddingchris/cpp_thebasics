/*!
 * @file      listing7.cpp
 * @brief     Declaration of a bidimensional array
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-07-21
 * 
 * @copyright GNU General Public License v3.0 
 */

datatype arrayName [numberOfRows][numberOfColumns];