/*!
 * @file      listing2.cpp
 * @brief     size() and capacity()
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-04
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <vector>                                                   // including vector class

int main()
{
    std::vector<char>charVector{'H','i', '!'};

    std::cout << "Size: " << charVector.size() << std::endl;         // number of elements
    std::cout << "Capacity: " << charVector.capacity() << std::endl; // size of intVector given in number of elements
    std::cout << "Max size: " << charVector.max_size() << std::endl; // maximum size of intVector

    return 0;
}