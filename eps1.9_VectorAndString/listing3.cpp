/*!
 * @file      listing3.cpp
 * @brief     push_back() and pop_back()
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-04
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <vector>                              // including vector class

int main()
{
    std::vector<int> intVector{ 0, 1, 2, 3, 4 };

    int sizeVec = intVector.size();         // number of elements
    int valueVec = intVector.back();        // value of the last element

    std::cout << "size: " << sizeVec << std::endl;
    std::cout << "value at the end: " << valueVec << std::endl;

    intVector.push_back(5);                 // adding an element to the end

    sizeVec = intVector.size();
    valueVec = intVector.back();

    std::cout << "new size: " << sizeVec << std::endl;
    std::cout << "New value at the end: " << valueVec << std::endl;

    intVector.pop_back();                   // removing an element from the end

    sizeVec = intVector.size();
    valueVec = intVector.back();

    std::cout << "size after pop_back(): " << sizeVec<< std::endl;
    std::cout << "value after pop_back():  " << valueVec << std::endl;

    std::cout << "value after pop_back():  " << intVector.at(sizeVec-1) << std::endl;

    return 0;
}