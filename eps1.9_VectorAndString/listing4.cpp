/*!
 * @file      listing4.cpp
 * @brief     insert() and erase()
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-04
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <vector>                              // including vector class

int main()
{
    int position= 3;
    std::vector<int> intVector{ 0, 1, 2, 3, 4 };

    int sizeVec = intVector.size();
    int valueVec = intVector.at(position);

    std::cout << "size: " << sizeVec << std::endl;
    std::cout << "value at position: " << valueVec << std::endl;

    intVector.insert(intVector.begin()+position, 1, 6);             // inserting an element

    sizeVec = intVector.size();
    valueVec = intVector.at(position);

    std::cout << "new size: " << sizeVec << std::endl;
    std::cout << "New value at position: " << valueVec << std::endl;

    intVector.erase(intVector.begin()+position);                     // erasing an element

    sizeVec = intVector.size();
    valueVec = intVector.at(position);

    std::cout << "size after erase(): " << sizeVec<< std::endl;
    std::cout << "value after erase():  " << valueVec << std::endl;

    return 0;
}