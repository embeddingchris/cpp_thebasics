/*!
 * @file      listing5.cpp
 * @brief     Multi-dimensional vector
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-04
 * 
 * @copyright GNU General Public License v3.0 
 */

// declaration of a multi-dimensional array of integers
int[][] mulitArray;

// declaration of a multi-dimensional vector of integers
std::vector<std::vector<int>> mulitVector;