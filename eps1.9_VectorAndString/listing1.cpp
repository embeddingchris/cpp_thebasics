/*!
 * @file      listing1.cpp
 * @brief     Declaring and initializing a vector
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-04
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <vector>                              // including vector class

int main()
{
    std::vector<int> intVector(3);             // declaring a vector of integers
    intVector[2] = 76;                         // assigning a value to an element

    std::cout << intVector[2] << std::endl;

    std::vector<char>charVector{'H','i', '!'}; // initialize a vector of characters
    std::cout << charVector.at(0)<< std::endl;

    return 0;
}