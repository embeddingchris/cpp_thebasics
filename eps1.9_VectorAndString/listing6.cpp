/*!
 * @file      listing6.cpp
 * @brief     C++ string
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-08-04
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <string>                                       // including string class

int main()
{
    std::string greeting ("Hello there!");              // defining a string variable
    std::cout << greeting << std::endl;                 // showing value of string variable

    std::cout << "What is your name?" << std::endl;     // showing string literal

    std::string name;                                   // declaring a string variable
    std::cin >> name;                                   // assign a stream to a string variable

    std::cout << "Hi "<< name << "," << std::endl;
    std::cout << "your name has "<< name.length()       // length of a string
              << " characters" << std::endl;

    return 0;
}