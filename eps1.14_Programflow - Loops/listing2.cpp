/*!
 * @file      listing2.cpp
 * @brief     While loop
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-13
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <stdlib.h>
#include <string>

int main()
{
    int guessable {rand() % 10};
    int guess {0};
    
    std::string retry {"y"};
    std::cout << "Guess a number between 0 and 1 " << std::endl;
    
    while(retry == "y")             // while loop
    {
        std::cout << "Your guess: ";
        std::cin >> guess;

        if(guessable > guess)
        {
            std::cout << "I'm afraid you missed! Your number is too small. " << std::endl;
        }
        else if(guessable < guess)
        {
            std::cout << "I'm afraid you missed! Your number is too large. " << std::endl;
        }
        else
        {
            std::cout << "Great! "<< guess << " is right." << std::endl;
            guessable = rand() % 10;
        }
        
        std::cout << "New try? (y/n)" << std::endl;
        std::cin >> retry;
    }

    std::cout << "See you!" << std::endl;
    
    return 0;
}