/*!
 * @file      listing7.cpp
 * @brief     Multidimensional array
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-13
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    const int columns = 3;
    const int rows = 2;
    
    int arrayMulti [rows][columns] { {1,2,3},{4,5,6} };
    
    for (int i = 0; i < rows; ++i)
    {
        // iterate integers in each row (columns)
        for (int j = 0; j < columns; ++j)
        {
            std::cout << "Element[" << i << "][" << j << "] = " << arrayMulti[i][j] << std::endl;
        }
    }

    return 0;
}