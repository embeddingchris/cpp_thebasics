/*!
 * @file      listing6.cpp
 * @brief     Nested loop
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-13
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    const int array1Len = 3;
    const int array2Len = 2;

    int array1[array1Len] {26, -8, 0};
    int array2[array2Len] {19, -5};

    std::cout << "Multiplying each integer in array1 by each integer in array2:" << std::endl;

    for(int i = 0; i < array1Len; ++i)
    {
        for(int j = 0; j < array2Len; ++j)
        {
            std::cout << array1[i] << " x " << array2[j] << " = " << array1[i] * array2[j] << std::endl;
        }
    }

    return 0;
}