/*!
 * @file      listing5.cpp
 * @brief     For loop
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-13
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <stdlib.h>

int main()
{
    int guessable {rand() % 10};
    int guess {0};
    int numberOfGuesses {3};
    
    std::cout << "Guess a number between 0 and 1 " << std::endl;

    for(int i = 0; i < numberOfGuesses; i++)                // for loop
    {
        std::cout << numberOfGuesses - i << " guess left" << std::endl;
        std::cout << "Your guess: ";
        std::cin >> guess;

        if(guessable > guess)
        {
            std::cout << "I'm afraid you missed! Your number is too small. " << std::endl;
        }
        else if(guessable < guess)
        {
            std::cout << "I'm afraid you missed! Your number is too large. " << std::endl;
        }
        else
        {
            std::cout << "Great! "<< guess << " is right." << std::endl;
            break;
        }
    }

    std::cout << "See you!" << std::endl;

    return 0;
}