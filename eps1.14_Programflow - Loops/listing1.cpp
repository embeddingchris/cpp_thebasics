/*!
 * @file      listing1.cpp
 * @brief     Goto instruction
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-10-13
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <stdlib.h>
#include <string>

int main()
{
    int guessable {rand() % 10};
    int guess {0};
    
    std::string retry {"y"};
    
    std::cout << "Guess a number between 0 and 1 " << std::endl;
    
    Start:                          // start label
    std::cout << "Your guess: ";
    std::cin >> guess;
    
    if(guessable > guess)
    {
        std::cout << "I'm afraid you missed! Your number is too small. " << std::endl;
    }
    else if(guessable < guess)
    {
        std::cout << "I'm afraid you missed! Your number is too large. " << std::endl;
    }
    else
    {
        std::cout << "Great! "<< guess << " is right." << std::endl;
        guessable = rand() % 10;
    }

    std::cout << "New try? (y/n)" << std::endl;
    std::cin >> retry;

    if(retry == "y")
    {
        goto Start;                 // goto instruction
    }
    else
    {
        std::cout << "See you!" << std::endl;
    }

    return 0;
}