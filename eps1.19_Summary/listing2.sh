#
# @file      listing2.cpp
# @brief     Compile and run
# @details
#
# @author    embeddingchris
# @date      2019-12-22
#
# @copyright GNU General Public License v3.0

#!/bin/bash

g++ -o listing1 listing1.cpp
./listing1
