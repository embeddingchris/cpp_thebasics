/*!
 * @file      listing7.cpp
 * @brief     Pointers and references
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-22
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int anInt{15};          // variable initialization
    int& refToInt = anInt;  // reference intialization
    int* pointToInt;        // pointer declaration
    pointToInt = &anInt;    // pointer assignment

    std::cout << "Value of variable = " << anInt << std::endl;
    std::cout << "Address of variable = " << &anInt << std::endl;

    std::cout << "Value of reference = " << refToInt << std::endl;
    std::cout << "Address of reference = " << &refToInt << std::endl;

    std::cout << "Value of pointer = " << *pointToInt << std::endl;
    std::cout << "Address of pointer = " << pointToInt << std::endl;
}
