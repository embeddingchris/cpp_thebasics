/*!
 * @file      listing4.cpp
 * @brief     Conditional execution with if ... else
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-22
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>
#include <stdlib.h>

int main()
{
    bool condition = rand() % 2;
    std::cout << "condition is " << condition << std::endl;

    if(condition)
    {
        std::cout << "condition is true" << std::endl;
    }
    else
    {
        std::cout << "condition is false" << std::endl;
    }

    return 0;
}
