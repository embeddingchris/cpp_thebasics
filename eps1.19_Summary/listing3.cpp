/*!
 * @file      listing3.cpp
 * @brief     Input and output
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-22
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
  struct {
    char* initials{new char[3]};
    float floatNumber;
    int intNumber;
  } inputStorage;

  std::cout << "Hi, what are your initials?" << std::endl;
  std::cin >> inputStorage.initials;

  std::cout << "And now, please tell me a number between 2.0 and 3.0" << std::endl;
  std::cin >> inputStorage.floatNumber;

  inputStorage.intNumber = inputStorage.floatNumber;

  std::cout << "Hey " << inputStorage.initials << ", be careful with the data types, otherwise your " << inputStorage.floatNumber << " will quickly turn into a " << inputStorage.intNumber << "!" << std::endl;

  return 0;
}
