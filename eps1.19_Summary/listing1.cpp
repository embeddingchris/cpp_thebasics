/*!
 * @file      listing1.cpp
 * @brief     Hello world
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-22
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    std::cout << "Hello World!" << std::endl;
    return 0;
}
