/*!
 * @file      listing5.cpp
 * @brief     Comments
 * @details   Show the use of comments in C++
 * 
 * @author    embeddingchris
 * @date      2019-04-28
 * 
 * @copyright GNU General Public License v3.0 
 */

// This is a comment; the compiler will ignore it

/* This is a comment
and it spans two lines */

// listing4: comment styles