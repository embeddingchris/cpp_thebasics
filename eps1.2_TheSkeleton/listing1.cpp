/*!
 * @file      listing1.cpp
 * @brief     Hello world example
 * @details   Anatomy of a C++ software program
 * 
 * @author    embeddingchris
 * @date      2019-04-28
 * 
 * @copyright GNU General Public License v3.0 
 */

// Preprocessor Directive
#include <iostream>

// The Body - Start of the Program
// main() Function
int main()
{
    // Output
    std::cout << "Hello World!" << std::endl;

    // Return Value
    return 0;
}