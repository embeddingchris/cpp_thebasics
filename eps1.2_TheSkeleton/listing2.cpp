/*!
 * @file      listing2.cpp
 * @brief     Include directive
 * @details   Example of an include directive
 * 
 * @author    embeddingchris
 * @date      2019-04-28
 * 
 * @copyright GNU General Public License v3.0 
 */

#include "../relative/path/filename"