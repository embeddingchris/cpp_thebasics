/*!
 * @file      listing6.cpp
 * @brief     Namespace
 * @details   Show the use of namespace in C++
 * 
 * @author    embeddingchris
 * @date      2019-04-28
 * 
 * @copyright GNU General Public License v3.0 
 */

// Preprocessor Directive
#include <iostream>

// Body
// namespace
using namespace std;

// main() function with parameters
int main(int argc, char* argv[])
{
    // Output
    cout << "Hello World!" << endl;

    // Return Value
    return 0;
}