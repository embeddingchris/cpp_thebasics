/*!
 * @file      listing3.cpp
 * @brief     Function parameters
 * @details   Show the main function and its arguments
 * 
 * @author    embeddingchris
 * @date      2019-04-28
 * 
 * @copyright GNU General Public License v3.0 
 */

int main (int argc, char* argv[])
{
    return 0;
}