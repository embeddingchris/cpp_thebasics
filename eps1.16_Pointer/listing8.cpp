/*!
 * @file      listing8.cpp
 * @brief     Array declaration
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

int anArray[25];