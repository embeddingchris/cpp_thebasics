/*!
 * @file      listing10.cpp
 * @brief     Memory allocation fail
 * @details   Allocation of memory will fail, because the data varaible is to big
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

int main()
{
    // request a large number of memory space
    int* pointsToLargeArray = new int [0x1fffffffff];
    
    // use the allocated memory
    delete[] pointsToLargeArray;
    
    return 0;
}