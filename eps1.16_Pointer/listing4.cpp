/*!
 * @file      listing4.cpp
 * @brief     Access to the value
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    // decalring a variable
    int intVar = 12;
    
    // declaring a pointer to type int and initialize to the address
    int* pointToInt = &intVar;
    
    std::cout << "Value of intVar = " << intVar << std::endl;
    std::cout << "Address of intVar = " << &intVar << std::endl;
    std::cout << "Value of pointToInt: " << pointToInt << std::endl;
    std::cout << "Stored value at the address pointToInt points to: " << *pointToInt << std::endl;
    
    return 0;
}