/*!
 * @file      listing3.cpp
 * @brief     Using a pointer to store an address
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    // decalring a variable
    int intVar = 12;
    
    // declaring a pointer to type int and initialize to the address
    int* pointToInt = &intVar;
    
    std::cout << "Value of integer variable is: " << intVar << std::endl;
    std::cout << "Value of integer pointer is: " << pointToInt << std::endl;
    
    return 0;
}