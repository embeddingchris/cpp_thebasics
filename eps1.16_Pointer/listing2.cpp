/*!
 * @file      listing2.cpp
 * @brief     Determining the memory address
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int intVar = 12;
    const double doubleConst = 7.56;

    // get the value
    std::cout << "Value of integer variable is: " << intVar << std::endl;
    std::cout << "Size of integer variable is: " << sizeof(intVar) << " Byte" << std::endl;
    std::cout << "Value of double constant is: " << doubleConst << std::endl;
    std::cout << "Size of double constant is: " << sizeof(doubleConst) << " Byte" << std::endl;

    // Get the address in memory
    std::cout << "Integer variable is located at: " << &intVar << std::endl;
    std::cout << "Double constant is located at: " << &doubleConst << std::endl;

    return 0;
}