/*!
 * @file      listing9.cpp
 * @brief     Requesting memory with "new" operator
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

// request memory for one element
int* pointToNewInt = new int;

// request memory for a block of five integers
int* pointToFiveInt = new int[5];