/*!
 * @file      listing1.cpp
 * @brief     Pointer declarartion
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

// PointedType * PointerVariableName = reserved memory address to point to nowhere;
int * pointToInt = nullptr;