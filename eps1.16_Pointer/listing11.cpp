/*!
 * @file      listing11.cpp
 * @brief     std::nothrow
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    // request a large number of memory space
    int* pointsToLargeArray = new(std::nothrow) int [0x1fffffffff];
    
    if (pointsToLargeArray == nullptr)
    {
        std::cout << "Allocation returned nullptr\n";
    }
    else
    {
        // use the allocated memory
        delete[] pointsToLargeArray;
    }
    
    return 0;
}