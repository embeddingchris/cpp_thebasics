/*!
 * @file      listing7.cpp
 * @brief     Incrementing / decrementing a pointer
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int anInt = 32;
    int* pointsToInt = &anInt;

    std::cout << "Address of anInt " << pointsToInt << std::endl;

    pointsToInt++;                      // incrementing a pointer
    std::cout << "Increased pointer " << pointsToInt << std::endl;

    pointsToInt--;                      // decrementing a pointer
    std::cout << "Decreased pointer " << pointsToInt << std::endl;

    return 0;
}