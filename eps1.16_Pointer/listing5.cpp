/*!
 * @file      listing5.cpp
 * @brief     Assigning a value to a pointer
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    int yourAge;
    double yourHeight;

    int* pointsToAge = &yourAge;
    double* pointsToHeight = &yourHeight;

    // store input at the memory pointed to
    std::cout << "How old are you?" << std::endl;
    std::cin >> *pointsToAge;

    std::cout << "How tall are you (in m)?" << std::endl;
    std::cin >> *pointsToHeight;

    // displaying the values of the variables
    std::cout << "You are "<< yourAge << " years old and "<< yourHeight << " meter tall." << std::endl;
    
    return 0;
}