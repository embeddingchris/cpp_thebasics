/*!
 * @file      listing6.cpp
 * @brief     Size of a pointer
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
    std::cout << "Size of char: " << sizeof(char) << std::endl;
    std::cout << "Size of pointer to char: " << sizeof(char*) << std::endl << std::endl;

    std::cout << "Size of int: " << sizeof(int) << std::endl;
    std::cout << "Size of pointer to int: " << sizeof(int*) << std::endl << std::endl;

    std::cout << "Size of double: " << sizeof(double) << std::endl;
    std::cout << "Size of pointer to double: " << sizeof(double*) << std::endl;
    
    return 0;
}