/*!
 * @file      listing12.cpp
 * @brief     Releasing memory with "delete" operator
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-10
 * 
 * @copyright GNU General Public License v3.0 
 */

// releasing memory of one element
delete pointToNewInt;

// releasing memory of a block of several elements
delete[] pointToFiveInt;