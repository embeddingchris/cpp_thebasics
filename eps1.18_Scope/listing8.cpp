/*!
 * @file      listing8.cpp
 * @brief     Different types of function arguments
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-08
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

// function argument as copy
int myFunction(int argument)
{
  // cannot see local variable of main function
  // std::cout << "Value of argument in main: << value;
  argument = 7; // can only see local copy of it, passed as argument

  std::cout << "Value of argument in my function: "
            << argument << std::endl;
  std::cout << "Address of argument in my function: "
            << &argument << std::endl;

  int localInt = 5;
  int localSum = localInt + argument;

  return localSum;
}

// function argument as reference
int myFunctionReference(int &argument)
{
  // cannot see local variable of main function
  // std::cout << "Value of argument in main: << value;
  argument = 7; // can only see the reference to it, passed as argument
  // But I can change the value of it as I have access to the memory address

  std::cout << "Value of argument in my function as reference: "
            << argument << std::endl;
  std::cout << "Address of argument in my function as reference: "
            << &argument << std::endl;

  int localInt = 5;
  int localSum = localInt + argument;

  return localSum;
}

// function argument as pointer
int myFunctionPointer(int *argument)
{
  // cannot see local variable of main function
  // std::cout << "Value of argument in main: << value;
  *argument = 5; // can only see the a pointer to it, passed as argument
  // But I can change the value of it as I have access to the memory address

  std::cout << "Value of argument in my function as pointer: "
            << *argument << std::endl;
  std::cout << "Address of argument in my function as pointer: "
            << argument << std::endl;

  int localInt = 5;
  int localSum = localInt + *argument;

  return localSum;
}

int main()
{
  int value = 5;

  std::cout << "Value of argument in main: " << value << std::endl;
  std::cout << "Address of argument in main: "<< &value << std::endl;

  int returnValue = myFunction(value);
  std::cout << "My function returns " << returnValue << std::endl << std::endl;

  std::cout << "Value of argument in main: " << value << std::endl;
  std::cout << "Address of argument in main: "<< &value << std::endl;

  returnValue = myFunctionReference(value);
  std::cout << "My function returns " << returnValue << std::endl << std::endl;

  std::cout << "Value of argument in main: " << value << std::endl;
  std::cout << "Address of argument in main: "<< &value << std::endl;

  returnValue = myFunctionPointer(&value);
  std::cout << "My function returns " << returnValue << std::endl << std::endl;
  std::cout << "Value of argument in main: " << value << std::endl;

  return 0;
}
