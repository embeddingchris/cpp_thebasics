/*!
 * @file      listing6.cpp
 * @brief     Namespace variable
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-08
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

namespace myNamespace{
 int globalInt = 5;
}

int main()
{
  {
  int localInt = 5;

    std::cout << "I can see globalInt(" << myNamespace::globalInt << ")" << std::endl;
    std::cout << "I can see localInt("  << localInt  << ")" << std::endl;


    int globalInt = 7;

    std::cout << "And I can see another globalInt(" << globalInt << "), declared in this scope" << std::endl;
    std::cout << "Meanwhile, I can still see globalInt(" << myNamespace::globalInt << ") with its namespace" << std::endl;
  }

  return 0;
}
