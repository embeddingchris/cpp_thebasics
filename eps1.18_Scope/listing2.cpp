/*!
 * @file      listing2.cpp
 * @brief     In scope
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-08
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
  {
    int anInt = 7;

    std::cout << "I can see anInt(" << anInt << ")" << std::endl;

    {
      std::cout << "I can still see anInt(" << anInt << ")" << std::endl;  
    }
  }
  
  return 0;
}