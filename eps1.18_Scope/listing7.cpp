/*!
 * @file      listing7.cpp
 * @brief     Function scope
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-08
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int globalInt = 3;

int myFunction(int arg)
{
  int localInt = 5;
  int localSum = localInt + globalInt + arg;

  return localSum;
}

int main()
{
  int value = 5;
  std::cout << "Function returns: " << myFunction(value) << std::endl;
  
  return 0;
}
