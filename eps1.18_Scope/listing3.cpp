/*!
 * @file      listing3.cpp
 * @brief     Not in scope
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-08
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
  {
    int anInt = 7;
  }
  
  // will not compile; anInt is out of scope
  std::cout << "I can not see anInt(" << anInt << ")" << std::endl;

  return 0;
}