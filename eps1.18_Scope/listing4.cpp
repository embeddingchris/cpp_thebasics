/*!
 * @file      listing4.cpp
 * @brief     Variable name in scope
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-08
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
  int anInt = 3;

  {
    std::cout << "I can see anInt(" << anInt << ")" << std::endl;

    int anInt = 7;

    std::cout << "I can still see anInt(" << anInt << "), right?" << std::endl;
  }
  
  std::cout << "I am sure anInt(" << anInt << ") is still the same" << std::endl;

  return 0;
}