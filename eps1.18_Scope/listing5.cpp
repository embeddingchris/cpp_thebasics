/*!
 * @file      listing5.cpp
 * @brief     Global variable
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-12-08
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int globalInt = 3;

int main()
{
  {
    int localInt = 5;

    std::cout << "I can see globalInt(" << globalInt << ")" << std::endl;
    std::cout << "I can see localInt("  << localInt  << ")" << std::endl;
 

    int globalInt = 7;

    std::cout << "I can still see globalInt(" << globalInt << "), right?" << std::endl;
  }
  
  std::cout << "I can see globalInt(" << globalInt << ")" << std::endl;
  // std::cout << "I can not see localInt("  << localInt  << ")" << std::endl;
  
  return 0;
}