# cpp_TheBasics

This is the accompanying repository to my article series [C++: The Basics](https://embeddingchris.com/posts/programming/short-introduction-en/).
If you follow my blog [embeddingchris.com](https://embeddingchris.com) and the post series, you can easily find the explained examples here.

## Getting Started

Just clone the repository and open the file with the editor / IDE of your choice.

- run the code
- look at the code
- play with the code

This is a good way to work with the examples.

And please feel free to make comments, improvements and ask questions.

## Episode Guide

- [eps1.0_Prolog](https://embeddingchris.com/posts/c++/short-introduction-en/)
- [eps1.1_GettingStarted](https://embeddingchris.com/posts/c++/getting-started-en/)
- [eps1.2_TheSkeleton](https://embeddingchris.com/posts/c++/the-skeleton-en/)
- [eps1.3_VariablesAndConstants](https://embeddingchris.com/posts/c++/variables-and-constants-en/)
- [eps1.4_Datatypes](https://embeddingchris.com/posts/c++/datatypes-en/)
- [eps1.5_InputOutput](https://embeddingchris.com/posts/c++/input-output-en/)
- [eps1.6_Enumeration](https://embeddingchris.com/posts/c++/enumeration-en/)
- [eps1.7_Struct](https://embeddingchris.com/posts/c++/struct-en/)
- [eps1.8_Array](https://embeddingchris.com/posts/c++/array-en/)
- [eps1.9_VectorAndString](https://embeddingchris.com/posts/c++/vector-and-string-en/)
- [eps1.10_ExpressionsAndStatements](https://embeddingchris.com/posts/c++/expressions-and-statements-en/)
- [eps1.11_Operators](https://embeddingchris.com/posts/c++/operators-p1-en/)
- [eps1.12_Operators-p2](https://embeddingchris.com/posts/c++/operators-p2-en/)
- [eps1.13_Programflow](https://embeddingchris.com/posts/c++/programflow-en/)
- [eps1.14_Programflow - Loops](https://embeddingchris.com/posts/c++/programflow-loops-en/)
- [eps1.15_Functions](https://embeddingchris.com/posts/c++/functions-en/)
- [eps1.16_Pointer](https://embeddingchris.com/posts/c++/pointer-en/)
- [eps1.17_References](https://embeddingchris.com/posts/c++/references-en/)
- [eps1.18_Scope](https://embeddingchris.com/posts/c++/scope-en/)

## Author

embeddingchris - Initial work - [embeddingchris@gmail.com](embeddingchris@gmail.com)

## License

This project is available under the GNU GPL v3 license. See the LICENSE file for more info.

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

