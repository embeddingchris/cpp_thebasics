/*!
 * @file      listing2.cpp
 * @brief     Declaring a function
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-24
 * 
 * @copyright GNU General Public License v3.0 
 */

// returnType FunctionName (type parameterName)
int toSquare(int number);