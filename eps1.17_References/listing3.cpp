/*!
 * @file      listing3.cpp
 * @brief     Reference as function parameter
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-24
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int toSquare(int number)
{
    std::cout << "(memory address of argument = " << &number << ")" << std::endl;

    number *= number;
    return number;
}

void toSquareRef(int& number)
{
    std::cout << "(memory address of argument = " << &number << ")" << std::endl;
    number *= number;
}

int main()
{
    int number = 10;
    std::cout << "number = " << number << std::endl;
    std::cout << "memory address of number = " << &number << std::endl;

    std::cout << "The square of " << number << std::endl;
    std::cout << "is: " << toSquare(number) << std::endl;

    std::cout << "The square of " << number << std::endl;
    std::cout << "is: ";
    toSquareRef(number);
    std::cout << number << std::endl;

    return 0;
}