/*!
 * @file      listing1.cpp
 * @brief     Reference declaration
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-24
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

int main()
{
  int anInt{15};
  std::cout << "anInt = " << anInt << std::endl;
  std::cout << "memory address of anInt = " << &anInt << std::endl;
  
  int& refToInt = anInt;
  std::cout << "reference to anInt = " << refToInt << std::endl;
  std::cout << "memory address of reference to anInt = " << &refToInt << std::endl;
  
  int& anotherRef = refToInt;
  std::cout << "anotherRef = " << anotherRef << std::endl;
  std::cout << "anotherRef is at address: " << &anotherRef << std::endl;
  
  return 0;
}