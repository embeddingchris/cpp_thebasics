/*!
 * @file      listing4.cpp
 * @brief     Const reference
 * @details   
 * 
 * @author    embeddingchris
 * @date      2019-11-24
 * 
 * @copyright GNU General Public License v3.0 
 */

#include <iostream>

void toSquareRef(const int& number, int& result)
{
    result = number*number;
    // number *= number;
}

int main()
{
    int number = 0;
    int result = 0;

    std::cout << "Enter a number: ";
    std::cin >> number;

    toSquareRef(number, result);

    std::cout << "The square of " << number << std::endl;
    std::cout << "is: " << result << std::endl;
    
    return 0;
}